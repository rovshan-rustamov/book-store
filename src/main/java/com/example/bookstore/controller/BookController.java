package com.example.bookstore.controller;

import com.example.bookstore.dto.request.BookRequest;
import com.example.bookstore.dto.response.StudentResponse;
import com.example.bookstore.service.BookService;
import jakarta.persistence.GeneratedValue;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @PostMapping("/create-book") //TODO ask Elmir void vs ResponseEntity
    public void createBook(@RequestBody BookRequest bookRequest){
        bookService.createBook(bookRequest);
    }

    @GetMapping("/{bookId}/readers")
    public ResponseEntity<List<StudentResponse>> getStudentsByBookId(@PathVariable Long bookId){
        return ResponseEntity.ok(bookService.getStudentsByBookId(bookId));
    }

}
