package com.example.bookstore.controller;

import com.example.bookstore.dto.LoginResponse;
import com.example.bookstore.dto.request.StudentRequest;
import com.example.bookstore.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @PostMapping("/register")
    public ResponseEntity<LoginResponse> registerStudent(@RequestBody StudentRequest studentRequest){
        return ResponseEntity.ok(studentService.register(studentRequest));
    }

    @PutMapping("/login")
    public ResponseEntity<LoginResponse> loginStudent(@RequestBody LoginResponse loginResponse){
        return ResponseEntity.ok(studentService.login(loginResponse));
    }

    /* TODO ask Elmir if login response and login request good for two entities the same class

    */
}
