package com.example.bookstore.controller;

import com.example.bookstore.dto.LoginRequest;
import com.example.bookstore.dto.LoginResponse;
import com.example.bookstore.dto.request.AuthorRequest;
import com.example.bookstore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @PostMapping("/register")
    public ResponseEntity<LoginResponse> registerAuthor(@RequestBody AuthorRequest authorRequest){
       return ResponseEntity.ok(authorService.register(authorRequest));
    }

    @PutMapping("/login")
    public ResponseEntity<LoginResponse> loginAuthor(@RequestBody LoginRequest loginRequest){
        return ResponseEntity.ok(authorService.login(loginRequest));
    }



}
