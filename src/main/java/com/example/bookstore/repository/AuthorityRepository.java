package com.example.bookstore.repository;

import com.example.bookstore.model.authority.Authority;
import com.example.bookstore.model.authority.CustomGrantedAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<CustomGrantedAuthority, Long> {
    Optional<CustomGrantedAuthority> findByAuthority(Authority authority);
}
