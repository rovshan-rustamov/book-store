package com.example.bookstore.dto.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorRequest {
    String name;
    Integer age;
    String username;
    String password;
    String repeatedPassword;
}
