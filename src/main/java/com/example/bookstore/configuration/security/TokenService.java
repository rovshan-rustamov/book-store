package com.example.bookstore.configuration.security;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TokenService {

    static final String BEARER = "Bearer ";
    static final String AUTHORITY_CLAIM = "authority";
    static final String AUTHORIZATION = "Authorization";
    final ModelMapper modelMapper;
    final JwtService jwtService;

    public Optional<Authentication> getAuthentication(HttpServletRequest request){
        return Optional.ofNullable(request.getHeader(AUTHORIZATION))
                .filter(header -> isBearerAuth(header))
                .flatMap( header2 -> getAuthenticationBearer(header2));
    }

    private Optional<Authentication> getAuthenticationBearer(String header) {

        String token = header.substring(BEARER.length()).trim();
        Claims claims =  jwtService.parseJwt(token);
        if (claims.getExpiration().before(new Date())) return Optional.empty();
        return Optional.of(getAuthenticationBearer(claims));
    }

    private Authentication getAuthenticationBearer(Claims claims) {

        List<?> authorities = claims.get(AUTHORITY_CLAIM, List.class);
        List<GrantedAuthority> authorityList = authorities
                .stream()
                .map( authority -> new SimpleGrantedAuthority(authority.toString()))
                .collect(Collectors.toList());
        JwtCredentials credentials = modelMapper.map(claims, JwtCredentials.class);
        User user = new User(credentials.getSub(), "", authorityList);

        return new UsernamePasswordAuthenticationToken(credentials, user, authorityList);

    }


    private boolean isBearerAuth(String header) {

        return header.toLowerCase().startsWith(BEARER.toLowerCase());
    }

}

