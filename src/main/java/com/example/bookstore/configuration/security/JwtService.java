package com.example.bookstore.configuration.security;

import com.example.bookstore.model.Author;
import com.example.bookstore.model.Student;
import com.example.bookstore.model.authority.CustomGrantedAuthority;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JwtService {

    static String AUTHORITY = "authority";
    static final String EMAIL = "email";
    static final String NAME = "name";

    @Value("${spring.security.key}")
    String secretKey;
    @Value("${security.token.expTime}")
    Long expTimeMinutes;
    Key key;

    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(secretKey);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseJwt(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueTokenForAuthor(Author author) {
        return Jwts.builder()
                .setHeader(Map.of("type", "JWT"))
                .setSubject(author.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(expTimeMinutes))))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim(AUTHORITY, author.getAuthorities().stream()
                        .map(CustomGrantedAuthority::getAuthority).collect(Collectors.toList()))
                .claim(NAME, author.getName())
                .compact();

    }

    public String issueTokenForStudent(Student student){
        return Jwts.builder()
                .setHeader(Map.of("type", "JWT"))
                .setSubject(student.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(expTimeMinutes))))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim(AUTHORITY, student.getAuthorities().stream()
                        .map(CustomGrantedAuthority::getAuthority).collect(Collectors.toList()))
                .claim(NAME, student.getName())
                .claim(EMAIL, student.getEmail())
                .compact();
    }


}
