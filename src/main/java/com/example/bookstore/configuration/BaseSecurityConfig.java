package com.example.bookstore.configuration;

import com.example.bookstore.configuration.security.AuthFilterConfigurerAdapter;
import com.example.bookstore.configuration.security.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class BaseSecurityConfig {
    private final TokenService tokenService;
// USER, ADMIN, STUDENT, AUTHOR

    @Bean
    public SecurityFilterChain configureFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors(AbstractHttpConfigurer::disable);
        httpSecurity.csrf(AbstractHttpConfigurer::disable);
        httpSecurity.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        httpSecurity.authorizeHttpRequests(auth -> auth.requestMatchers(POST,"/author/register").permitAll());
        httpSecurity.authorizeHttpRequests(auth -> auth.requestMatchers(PUT,"/author/login").permitAll());
        httpSecurity.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());

        httpSecurity.httpBasic(withDefaults());
        httpSecurity.apply(new AuthFilterConfigurerAdapter(tokenService));
        return httpSecurity.build();
    }
}
