package com.example.bookstore.service;

import com.example.bookstore.configuration.security.JwtService;
import com.example.bookstore.dto.LoginRequest;
import com.example.bookstore.dto.LoginResponse;
import com.example.bookstore.dto.request.AuthorRequest;
import com.example.bookstore.model.Author;
import com.example.bookstore.model.authority.CustomGrantedAuthority;
import com.example.bookstore.repository.AuthorRepository;
import com.example.bookstore.repository.AuthorityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.bookstore.model.authority.Authority.AUTHOR;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthorService {
    private final AuthorRepository authorRepository;
    private final AuthorityRepository authorityRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    public LoginResponse register(AuthorRequest authorRequest) {
        Optional<Author> authorOptional = authorRepository.findByUsername(authorRequest.getUsername());
        if (authorOptional.isPresent()) throw new RuntimeException("username already exists");
        CustomGrantedAuthority grantedAuthority = authorityRepository.findByAuthority(AUTHOR)
                .orElseThrow(()-> new RuntimeException("authority not fount"));
        if (!authorRequest.getPassword().equals(authorRequest.getRepeatedPassword())){
            throw new RuntimeException("passwords do not match");
        }
        Author author = Author.builder()
                .name(authorRequest.getName())
                .age(authorRequest.getAge())
                .username(authorRequest.getUsername())
                .password(bCryptPasswordEncoder.encode(authorRequest.getPassword()))
                .authorities(List.of(grantedAuthority))
                .build();
        authorRepository.save(author);

        return LoginResponse.builder()
                .jwt(jwtService.issueTokenForAuthor(author))
                .build();

    }

    public LoginResponse login(LoginRequest loginRequest) {
        log.trace("login method");
        Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        Optional<Author> author = authorRepository.findByUsername(loginRequest.getUsername());
        if (author.isPresent()) {
            return LoginResponse.builder()
                    .jwt(jwtService.issueTokenForAuthor(author.get()))
                    .build();
        }
        throw new RuntimeException("no user found");

    }
}
// TODO fix registration to not send jwt